#ifndef PROJECT_HELLO_WORLD_FREEFUNCTIONS_H
#define PROJECT_HELLO_WORLD_FREEFUNCTIONS_H

#include <set>
#include <string>
#include <map>
#include <vector>

class graph_type;
using std::string;

std::map<string, std::vector<string> > ParseArgs (std::vector<const char*> &argv);

string getLabel (const string &full_name);
string getPath (const string &full_name);
string getCanonicalDir (string full_name);
string getCanonical (string full_name);
string getWithoutParent (string full_name);
std::pair<string, string> getPairDir (string full_name);
int getLineNumber (string location);

// bicycle so bicycle
template <class Set, class UnaryPredicate>
void erase_if (Set& patient, UnaryPredicate pred)
{
	typename Set::iterator
			first = patient.begin(),
			last = patient.end();
	while (first!=last)
	{
		if (pred(*first))
		{
			first = patient.erase (first);
		}
		else
		{
			++first;
		}
	}
}



#endif /* PROJECT_HELLO_WORLD_FREEFUNCTIONS_H */

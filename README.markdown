        C++ Dependency Analyzer
        =======================

The purpose of this tool is analyzing of a translation unit header
dependencies and suggest improvements.




For start tool you can execution ./run.sh with first ./main.so (путь к бубилиотеке) and then any other your arguments for compiler. 

Example: 
./run.sh ./main.so tests/call_func/main.hpp
./run.sh ./main.so -std=c++11 -I/usr/lib/llvm-3.6/include  -DNDEBUG -D_GNU_SOURCE -D__STDC_CONSTANT_MACROS -D__STDC_FORMAT_MACROS -D__STDC_LIMIT_MACROS ./DependencyHandler.hpp -isystem /usr/

Then you will see real comand, what start tool.

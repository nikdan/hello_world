#ifndef DEPENDENCYHANDLER_H
#define DEPENDENCYHANDLER_H

#include "AbstractHandler.hpp"
#include "graph_type.hpp"

#include <clang/AST/Decl.h>
#include <clang/AST/DeclCXX.h>
#include <clang/AST/Type.h>
#include <clang/Basic/SourceLocation.h>
#include <clang/Basic/SourceManager.h>
#include <clang/Basic/Diagnostic.h>

#include <map>
#include <set>
#include <string>


using std::string;

class DependencyHandler : public AbstractHandler
{
public:
	DependencyHandler (clang::DiagnosticsEngine& DiagEng, clang::SourceManager &SM, bool verbose);
	void setVerbose (bool new_verbose);

	enum DpndKind {DefKind, DeclKind};
	typedef std::map<string, std::set<const clang::NamedDecl*> > DpndContainer;
	typedef std::pair<clang::SourceLocation, clang::SourceLocation> PairLoc;
	typedef std::pair<std::set<string>, std::set<string> > AddDeleteType;

	bool bind_dpnds (const clang::SourceLocation &Loc, const clang::NamedDecl *Decl, DpndKind Kind = DefKind);
	void setDpndsFromType (const clang::SourceLocation &Loc, clang::Type const * const v_Type, DpndKind Kind = DefKind);
	void setLinesNumber (const clang::SourceLocation &first, const clang::SourceLocation &last);
	const std::map<string, std::pair<int, int> > & getLinesNumber ();

	void eraseAddledDeclDpnds (const graph_type &file_graph);
	std::set<string> getDpnds (const string &file);
	void setNeedDelete (const string &file, const graph_type &ProxyMapping,
	                   const AddDeleteType &Deleters);

	void Print () override;

private:
	struct PairLocCmp
	{
		static const clang::SourceManager *p_SM;
		bool operator() (const PairLoc &first, const PairLoc &sec) const;
	};
public:

	std::set<PairLoc, PairLocCmp> MacroDep;

private:
	clang::SourceManager & _SM;
	DpndContainer decl_dpnds, def_dpnds;
	std::map<const clang::NamedDecl*, std::set<clang::SourceLocation> > BackSideMap;
	std::map<string, std::pair<int, int> > LinesNumberMap;
	std::map<string, std::pair<graph_type, AddDeleteType> > NeedDeleteMap;
	bool verbose;

	const clang::CXXRecordDecl* SubSetDpndsFromType (const clang::SourceLocation &Loc,
	                                                 clang::Type const * const v_Type,
	                                                 DpndKind Kind = DefKind);
	void PrintReplacementRecomendation ();
	void PrintAddDeleteRecomenadtion ();
	// void PrintForcedNeed ();
};


#endif /* DEPENDENCYHANDLER_H */

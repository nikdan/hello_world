
#include "Consumer.hpp"
#include "MyVisitor.hpp"
#include "PPCallbacks.hpp"
#include "DependencyHandler.hpp"
#include "GraphHandler.hpp"
#include "ProxyHandler.hpp"
#include "graph_type.hpp"

#include <iostream>

MyASTConsumer::MyASTConsumer (MyASTVisitor *vp_ASTV, clang::CompilerInstance *vp_CI, const string &MainFile)
	: vp_ASTV (vp_ASTV), vp_CI (vp_CI), MainFile (MainFile)
{
}

void MyASTConsumer::HandleTranslationUnit (clang::ASTContext &ctx)
{
	vp_ASTV->VisitDeclContext (ctx.getTranslationUnitDecl());

	my_PP* vp_PPC = dynamic_cast<my_PP*>(vp_CI->getPreprocessor().getPPCallbacks());
	std::unique_ptr<GraphHandler> vp_GraphHandler = vp_PPC->releaseGraph ();
	std::unique_ptr<ProxyHandler> vp_ProxyHandler = vp_PPC->releaseProxy ();
	std::unique_ptr<DependencyHandler> vp_DependencyHandler = vp_ASTV->releaseDpnd ();

	vp_GraphHandler->MinimizeGuard (vp_DependencyHandler->getLinesNumber ());
	vp_DependencyHandler->eraseAddledDeclDpnds (vp_GraphHandler->getGraph ());
	vp_ProxyHandler->CreateProxySubTree (vp_GraphHandler->getGraph());

	for (auto& file_el : vp_GraphHandler->getGraph())
	{
		if (isShadow(file_el.first))
		{
			continue;
		}
		const std::set<string> Dpnds {vp_DependencyHandler->getDpnds (file_el.first)};
		graph_type ProxyMapping {vp_ProxyHandler->CreateProxyMapping (Dpnds)};
		if ((MainFile == file_el.first) && (MainFile != vp_ProxyHandler->getParent()))
		{
			auto iter_file = vp_GraphHandler->getGraph().find (vp_ProxyHandler->getParent());
			if (iter_file != vp_GraphHandler->getGraph().end())
			{
				vp_ProxyHandler->MinimizeProxyMappingFromExist (ProxyMapping, iter_file->second);
			}
		}
		auto AddDelete
				(vp_ProxyHandler->MinimizeProxyMappingFromExist(ProxyMapping, file_el.second));
		if (vp_ProxyHandler->isProxy(file_el.first))
		{
			AddDelete.second.clear();
		}
		vp_DependencyHandler->setNeedDelete (file_el.first, ProxyMapping, AddDelete);
	}

	vp_GraphHandler->Print ();
	vp_DependencyHandler->Print ();
}

#ifndef BASE_H
#define BASE_H

template<class T1, class T2>
struct Base
{
	T1 t1;
	T2 t2;
};
#endif /* BASE_H */

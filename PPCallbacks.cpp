#include "PPCallbacks.hpp"
#include "FreeFunctions.hpp"

#include <clang/Basic/SourceManager.h>
#include <clang/Basic/SourceLocation.h>
#include <clang/Basic/FileManager.h>
#include <clang/Basic/Module.h>
#include <clang/Lex/MacroInfo.h>
#include <llvm/ADT/StringRef.h>
#include <llvm/Support/raw_ostream.h>
#include <iostream>
#include <iomanip>

using namespace clang;
using namespace std;

static std::string duct_tape (const clang::SourceLocation &SL, const clang::SourceManager &SM)
{
	std::string S;
	llvm::raw_string_ostream OS(S);
	SL.print(OS, SM);
	return OS.str();
}



void MyPragmaAllHandler::HandlePragma (Preprocessor &PP,
                                       PragmaIntroducerKind Introducer,
                                       Token &FirstToken)
{
	my_PP* vp_MPP = dynamic_cast<my_PP*>(PP.getPPCallbacks());
	vp_MPP->RemoveThisPragmaLocation (FirstToken.getLocation());
}



my_PP::my_PP (SourceManager &SM,
              std::unique_ptr<ProxyHandler> vp_ProxyHandler,
              std::unique_ptr<GraphHandler> vp_GraphHandler)
	: _SM{SM},
	  vp_ProxyHandler{std::move(vp_ProxyHandler)},
	  vp_GraphHandler{std::move(vp_GraphHandler)}
{
}

std::unique_ptr<GraphHandler> my_PP::releaseGraph ()
{
	return move(vp_GraphHandler);
}
std::unique_ptr<ProxyHandler> my_PP::releaseProxy ()
{
	return move(vp_ProxyHandler);
}



void my_PP::InclusionDirective (SourceLocation HashLoc,
                                const Token &IncludeTok,
                                StringRef FileName,
                                bool IsAngled,
                                CharSourceRange FilenameRange,
                                const FileEntry *File,
                                StringRef SearchPath,
                                StringRef RelativePath,
                                const Module *Imported)
{
	string name1 = getWithoutParent (_SM.getFilename(HashLoc).str());
	string name2 =  (File != nullptr) ? File->getName() : FileName.str();
	name2 = getWithoutParent (name2);
	vp_GraphHandler->AddLink (name1, name2);
}


void my_PP::FileChanged (SourceLocation Loc,
                         FileChangeReason Reason,
                         SrcMgr::CharacteristicKind FileType,
                         FileID PrevFID)
{
	string file {getWithoutParent(_SM.getFilename(Loc).str())};
	if (file.size())
	{
		vp_ProxyHandler->RegisterFile (file);
		if (Reason == SystemHeaderPragma)
		{
			vp_GraphHandler->decrIncl (file);
		}
	}
}



void my_PP::PragmaDirective (SourceLocation Loc, PragmaIntroducerKind Introducer)
{
	string full_name = getPairDir(getWithoutParent( duct_tape(Loc, _SM) )).first;
	size_t line = getLineNumber(full_name);
	vp_GraphHandler->insertPragmaLoc (getCanonicalDir(full_name), line);
}


void my_PP::RemoveThisPragmaLocation (SourceLocation Loc)
{
	string file = getWithoutParent (_SM.getFilename (Loc));
	if (!file.size())
	{
		printf("!file.size()");
		exit(1);
	}
	size_t line = getLineNumber( duct_tape(Loc, _SM) );
	vp_GraphHandler->ErasePragma (getCanonicalDir(file), line);
}


void my_PP::Endif (SourceLocation Loc, SourceLocation IfLoc)
{
	std::string filename = getWithoutParent (_SM.getFilename(Loc));
	int first = getLineNumber(getWithoutParent( duct_tape(IfLoc, _SM) ));
	int last = getLineNumber(getWithoutParent( duct_tape(Loc, _SM) ));
	vp_GraphHandler->setLinesNumber (filename, first, last);
}


void my_PP::Ifndef (SourceLocation Loc, const Token &MacroName, const MacroDirective *MD)
{
	string filename = getWithoutParent (_SM.getFilename(Loc).str());
	vp_GraphHandler->AddGuard (filename, MacroName.getIdentifierInfo()->getName().str());
}


void my_PP::Defined (const Token &MacroName, const MacroDirective *MD, SourceRange Range)
{
	string filename = getWithoutParent (_SM.getFilename(MacroName.getLocation()));
	vp_GraphHandler->AddGuard (filename, MacroName.getIdentifierInfo()->getName().str());
}


void my_PP::MacroUndefined (const Token &MacroName, const MacroDirective *MD)
{
	string filename = getWithoutParent (_SM.getFilename(MacroName.getLocation()));
	vp_GraphHandler->EraseGuard (filename, MacroName.getIdentifierInfo()->getName().str());
}


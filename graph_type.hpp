#ifndef PROJECT_HELLO_WORLD_GRAPH_TYPE
#define PROJECT_HELLO_WORLD_GRAPH_TYPE

#include <set>
#include <map>
#include <vector>
#include <string>

using std::string;
using std::vector;

class graph_type : public std::map<string, std::set<string> >
{
public:
	static std::set<string> Shadow;
	void CheckCycle (const string &file);
private:
	std::set<string> Before;
	void SubCheckCycle (const string &file, vector<string> &Stack);
};

bool isShadow (string file);



#endif

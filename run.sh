#!/bin/bash

declare -a paramaters
declare -a subparamaters
parameters=(clang++-3.4 -fsyntax-only -Xclang -load -Xclang)


for i in $@
do
    parameters+=($i)
    if [ "$i" == "$1" ]
    then
        parameters+=(-Xclang -plugin -Xclang dep_anal -Xclang -plugin-arg-dep_anal -Xclang --config  -Xclang -plugin-arg-dep_anal -Xclang ${1/"main.so"/"parameters.dat"})
    fi
done

#make -j 8;

echo ""
echo ${parameters[@]}
echo ""
"${parameters[@]}"

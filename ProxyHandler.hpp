
#ifndef PROXYHANDLER_H
#define PROXYHANDLER_H

#include "graph_type.hpp"
#include <vector>
#include <utility>

class ProxyHandler
{
public:
	ProxyHandler (const string & MainFile);
	const string & getParent () const;
	bool isProxy (const string &file) const;

	bool insertFile (const string &file);
	bool insertPath (const string &path);

	void RegisterFile (const string &file);
	void CreateProxySubTree (const graph_type& file_graph);

	graph_type CreateProxyMapping (const std::set<string> &Dpnds) const;

	std::pair< std::set<string>, std::set<string> >
	MinimizeProxyMappingFromExist (graph_type &ProxyMapping, std::set<string> Exist) const;

private:
	std::set<string> Proxys;
	std::set<string> ProxyPaths;
	std::pair<string, string> ChildParent;
	graph_type ProxySubTree;

	void ShellOfCreation (std::vector<graph_type::iterator>, const graph_type &file_graph);
	void SubCreation (std::set<string> &SubProxySet, const std::set<string> &SubFileSet,
	                  const graph_type &file_graph);

	std::vector<string> getParentProxys (const string &file) const;
	void MinimizeProxyMappingFromSelf (graph_type &ProxyMapping) const;
};



#endif /* PROXYHANDLER_H */


#include "MyVisitor.hpp"
#include "FreeFunctions.hpp"
#include "graph_type.hpp"
#include <clang/AST/Stmt.h>
#include <clang/AST/Type.h>
#include <clang/AST/TemplateBase.h>
#include <clang/Basic/SourceLocation.h>
#include <string>
#include <set>

using namespace clang;
using namespace std;

static std::string duct_tape (const clang::SourceLocation &SL, const clang::SourceManager &SM)
{
	std::string S;
	llvm::raw_string_ostream OS(S);
	SL.print(OS, SM);
	return OS.str();
}



MyASTVisitor::MyASTVisitor (SourceManager &SM, unique_ptr<DependencyHandler> vp_DependencyHandler)
	: _SM {SM},
	  vp_DependencyHandler {move(vp_DependencyHandler)}
{}


bool MyASTVisitor::VisitDeclContext (DeclContext *DC)
{
	if (DC)
	{
		DeclContext::decl_iterator DIt = DC->decls_begin(), DEnd = DC->decls_end();
		for ( ; DIt != DEnd; ++DIt)
		{
			if ( (*DIt != nullptr) && (!(*DIt)->isImplicit()) )
			{
				vp_DependencyHandler->setLinesNumber ((*DIt)->getLocStart(), (*DIt)->getLocEnd());
				if ((*DIt)->getLocation().isMacroID())
				{
					auto first = _SM.getExpansionLoc((*DIt)->getLocation());
					auto second = _SM.getSpellingLoc((*DIt)->getLocation());
					vp_DependencyHandler->MacroDep.insert (make_pair(first, second));
				}
				if (!TraverseDecl (*DIt))
					return false;
			}
		}
	}
	return true;
}


bool MyASTVisitor::VisitDeclaratorDecl(clang::DeclaratorDecl* D)
{
	if (D)
	{
		DependencyHandler::DpndKind Kind = (dynamic_cast<ParmVarDecl*>(D))
				? DependencyHandler::DeclKind : DependencyHandler::DefKind;

		vp_DependencyHandler->setDpndsFromType (D->getLocStart(), D->getType().getTypePtr(), Kind);
		const TypedefType *TpdfType = D->getType()->getAs<TypedefType>();
		if (TpdfType)
		{
			vp_DependencyHandler->bind_dpnds (D->getLocStart(), TpdfType->getDecl(), Kind);
		}
	}
	return true;
}


bool MyASTVisitor::VisitFunctionDecl(clang::FunctionDecl* D)
{
#if ((__clang_major__ == 3) && (__clang_minor__ >= 6))
	vp_DependencyHandler->setDpndsFromType (D->getLocStart(),
	                                        D->getReturnType().getTypePtr(),
	                                        DependencyHandler::DeclKind);
#else
	vp_DependencyHandler->setDpndsFromType (D->getLocStart(),
	                                        D->getResultType().getTypePtr(),
	                                        DependencyHandler::DeclKind);
#endif
	return true;
}
bool MyASTVisitor::VisitCXXMethodDecl(clang::CXXMethodDecl* D)
{
	vp_DependencyHandler->bind_dpnds (D->getLocation(), D->getParent(), DependencyHandler::DefKind);
	return true;
}

bool MyASTVisitor::VisitCXXRecordDecl (CXXRecordDecl *D)
{
	string from;
	if (D && D->hasDefinition())
	{
		D = D->getDefinition();
		if (!isShadow(from = getWithoutParent( duct_tape(D->getLocStart(), _SM) )))
		{
			CXXRecordDecl::base_class_iterator BaseIt = D->bases_begin();
			for ( ; BaseIt != D->bases_end(); ++BaseIt)
			{
				vp_DependencyHandler->setDpndsFromType (D->getLocStart(), BaseIt->getType().getTypePtr());
			}
		}
	}
	return true;
}


bool MyASTVisitor::VisitClassTemplateSpecializationDecl (ClassTemplateSpecializationDecl *D)
{
	string from;
	if (D && !isShadow(from = getWithoutParent( duct_tape(D->getLocStart(), _SM) )))
	{
		ClassTemplateDecl * BaseTempl = D->getSpecializedTemplate	();
		vp_DependencyHandler->bind_dpnds (D->getLocStart(), BaseTempl);

		llvm::ArrayRef<TemplateArgument> arraySpec {D->getTemplateInstantiationArgs().asArray()};
		for (TemplateArgument arg : arraySpec)
		{
			if (arg.getKind() == TemplateArgument::ArgKind::Type)
			{
				if (arg.getAsType().getTypePtr())
				{
					vp_DependencyHandler->setDpndsFromType (D->getLocStart(), arg.getAsType().getTypePtr());
				}
			}
		}
	}
	return true;
}


bool MyASTVisitor::VisitTypedefNameDecl (TypedefNameDecl *D)
{
	if (D)
	{
		vp_DependencyHandler->setDpndsFromType (D->getLocStart(), D->getUnderlyingType().getTypePtr(),
		                                        DependencyHandler::DeclKind);
	}
	return true;
}


bool MyASTVisitor::VisitCallExpr (CallExpr *E)
{
	string from;
	if (E && !isShadow(from = getWithoutParent( duct_tape(E->getLocStart(), _SM) )))
	{
		vp_DependencyHandler->bind_dpnds (E->getLocStart(), E->getDirectCallee());
		CallExpr::arg_iterator ArgIter = E->arg_begin();
		for ( ; ArgIter != E->arg_end(); ++ArgIter)
		{
			vp_DependencyHandler->setDpndsFromType (E->getLocStart(), ArgIter->getType().getTypePtr());
		}
	}
	return true;
}


bool MyASTVisitor::VisitExplicitCastExpr (ExplicitCastExpr *E)
{
	if (E)
	{
		vp_DependencyHandler->setDpndsFromType (E->getLocStart(), E->getTypeAsWritten().getTypePtr());
	}
	return true;
}


unique_ptr<DependencyHandler> MyASTVisitor::releaseDpnd()
{
	return std::move(vp_DependencyHandler);
}

.PHONY : clean all test
CXX = clang++-3.4
CXXFLAGS := $(shell llvm-config-3.4 --cxxflags) -g3 -std=c++11 -Wall -O2 -fPIC
LLVMLDFLAGS := $(shell llvm-config-3.4 --ldflags --libs )
LDFLAGS := -L/usr/lib/x86_64-linux-gnu
sources= $(wildcard *.cpp)
objects=$(patsubst %.cpp,%.o,$(sources))
CLANGLIBS =  -ldl -lz -lncurses -lLLVMMC -lclangASTMatchers	\
	-lclangBasic -lclangARCMigrate -lclangCodeGen 		\
	-lclangStaticAnalyzerFrontend -lclangAST -lclangLex 	\
	-lclangStaticAnalyzerCheckers -lclangAnalysis		\
	-lclangStaticAnalyzerCore -lclangSerialization		\
	-lclangParse -lclangSema  -lclangIndex -lclangEdit 	\
	-lclangDriver -lclangFrontend  -lclangFrontendTool	\
	-lclang -lLLVM-3.4 -lclangTooling -lclangFrontendTool   \
	-lclangFrontend -lclangDriver -lclangSerialization      \
	-lclangCodeGen -lclangParse -lclangSema                 \
	-lclangStaticAnalyzerFrontend				\
	-lclangStaticAnalyzerCheckers -lclangStaticAnalyzerCore \
	-lclangAnalysis -lclangARCMigrate -lclangRewriteCore 	\
	-lclangRewriteFrontend -lclangEdit -lclangAST 		\
	-lclangLex -lclangBasic -lclang

all: main.so

test: main.so
	./run.sh ./main.so tests/*/main.hpp 2>tmp.txt; diff tmp.txt file.log && rm tmp.txt

$(objects) : $(wildcard *.hpp) Makefile

main.so : $(objects)
	$(CXX) $(CXXFLAGS) -Wl,--no-undefined -shared -o $@  $^ -Wl,--start-group $(CLANGLIBS) $(LLVMLDFLAGS) $(LDFLAGS) -Wl,--end-group -pthread


clean:
	-rm *.o *.svg *.txt *.so *.gch *.plist trash.*

library : $(objects)

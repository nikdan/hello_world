
#include "ProxyHandler.hpp"
#include "FreeFunctions.hpp"
#include <algorithm>

#include <thread>
#include <functional>
#include <iostream>

using namespace std;

template <class T>
struct Searcher
{
	bool flag;
	const T *Set;
	Searcher(const T &Dpnds) : flag(true), Set(&Dpnds) {}
	bool operator() (const string &file)
	{
		bool is_finded = (Set->find(file) != Set->end());
		if (flag)
		{
			return is_finded;
		}
		return !is_finded;
	}
};



ProxyHandler::ProxyHandler (const string & MainFile)
{
	ChildParent.second = ChildParent.first = MainFile;
	if ("cpp" == MainFile.substr(MainFile.find_last_of(".") + 1))
	{
		string Parent = MainFile.substr(0, MainFile.find_last_of(".") + 1) + "hpp";
		ChildParent.second = Parent;
		Proxys.insert(Parent);
	}
}

const string & ProxyHandler::getParent () const
{
	return ChildParent.second;
}


bool ProxyHandler::isProxy (const string &file) const
{
	if ((Proxys.find(file) == Proxys.end())
	    && (ProxyPaths.find(getPath(file) + '/') == ProxyPaths.end()))
	{
		return false;
	}
	return true;
}


bool ProxyHandler::insertFile (const string &file)
{
	return Proxys.insert(file).second;
}

bool ProxyHandler::insertPath (const string &path)
{
	return ProxyPaths.insert(path).second;
}


void ProxyHandler::RegisterFile (const string & file)
{
	if (isProxy(file))
	{
		Proxys.insert (file);
	}
}


pair< set<string>, set<string> >
ProxyHandler::MinimizeProxyMappingFromExist (graph_type &ProxyMapping, set<string> Exist) const
{
	Searcher<graph_type> searcher_map {ProxyMapping};
	set<string>::const_iterator iter = Exist.begin();
	while (iter != Exist.end())
	{
		iter = find_if (Exist.begin(), Exist.end(), searcher_map);
		if (iter != Exist.end())
		{
			ProxyMapping.erase (*iter);
			iter = Exist.erase (iter);
		}
	}

	Searcher< set<string> > searcher_set {Exist};
	set<string> Intersection;
	set<string> ForceNeed;
	for (auto& file_el : ProxyMapping)
	{
		searcher_set.flag = true;
		auto& sub_set = file_el.second;
		bool found = find_if (sub_set.begin(), sub_set.end(), searcher_set) != sub_set.end();
		if (found)
		{
			searcher_set.flag = false;
			erase_if (sub_set, searcher_set);
			Intersection.insert (sub_set.begin(), sub_set.end());
		}
		else
		{
			ForceNeed.insert (file_el.first);
		}
	}

	MinimizeProxyMappingFromSelf (ProxyMapping);
	erase_if (ProxyMapping, [&ForceNeed](graph_type::value_type &Element)
	          {
		          bool can_del = false;
		          if (Element.second.size() == 1)
		          {
			          can_del = ForceNeed.find (Element.first) == ForceNeed.end();
		          }
		          return can_del;
	          });
	for (auto& file : Intersection)
	{
		Exist.erase (file);
	}

	return make_pair (ForceNeed, Exist);
}


graph_type ProxyHandler::CreateProxyMapping (const set<string> &Dpnds) const
{
	Searcher< set<string> > searcher {Dpnds};
	graph_type ProxyMapping;
	for (auto& need_file : Dpnds)
	{
		vector<string> Parents = getParentProxys (need_file);
		if (Parents.size())
		{
			bool found = (find_if (Parents.begin(), Parents.end(), searcher) != Parents.end());
			if (found)
			{
				searcher.flag = false;
				Parents.erase (remove_if(Parents.begin(), Parents.end(), searcher), Parents.end());
				searcher.flag = true;
			}
			ProxyMapping[need_file].insert (Parents.begin(), Parents.end());
		}
		else
		{
			ProxyMapping[need_file].insert (need_file);
		}
	}
	return ProxyMapping;
}



void ProxyHandler::CreateProxySubTree (const graph_type& file_graph)
{
	// std::function<vector<graph_type::iterator>
	//               (set<string>::iterator first, set<string>::iterator last)> createInputData =
	auto createInputData =
			[&file_graph, this] (set<string>::iterator first, set<string>::iterator last)
			{
				vector<graph_type::iterator> VectSet;
				for (; first != last; ++first)
				{
					auto file_iter = file_graph.find (*first);
					if ((file_iter != file_graph.end()) && (file_iter->second.size()))
					{
						VectSet.push_back (ProxySubTree.insert(make_pair(*first, graph_type::mapped_type())).first);
					}
				}
				return VectSet;
			};

	size_t const enough_big_graph_size = 500;
	if (file_graph.size() > enough_big_graph_size)
	{
		size_t const min_per_thread = 25;
		size_t const max_threads = (Proxys.size() + min_per_thread - 1) / min_per_thread;
		size_t const hardware_threads = std::thread::hardware_concurrency();
		size_t const num_threads = std::min (((hardware_threads != 0) ? hardware_threads : 2), max_threads);
		size_t const block_size = Proxys.size() / num_threads;

		std::vector<std::thread> threads (num_threads-1);
		auto block_start = Proxys.begin();
		for (size_t i = 0; i < (num_threads-1) ; ++i)
		{
			auto block_end = block_start;
			std::advance (block_end, block_size);
			threads[i] = std::thread (&ProxyHandler::ShellOfCreation, this,
			                          createInputData(block_start, block_end), std::ref(file_graph));
			block_start = block_end;
		}
		ShellOfCreation (createInputData(block_start, Proxys.end()), file_graph);
		std::for_each (threads.begin(), threads.end(), std::mem_fn(&std::thread::join));
	}
	else
	{
		ShellOfCreation (createInputData(Proxys.begin(), Proxys.end()), file_graph);
	}
}



// private:

void ProxyHandler::ShellOfCreation (vector<graph_type::iterator> VectProxy,
                                    const graph_type &file_graph)
{
	for (auto& ProxyIter : VectProxy)
	{
		SubCreation (ProxyIter->second, file_graph.find(ProxyIter->first)->second, file_graph);
	}
}

void ProxyHandler::SubCreation (std::set<string> &SubProxySet, const std::set<string> &SubFileSet,
                                const graph_type &file_graph)
{
	for (auto& sub_file : SubFileSet)
	{
		if ((!isProxy(sub_file)) && (SubProxySet.insert(sub_file).second))
		{
			auto file_iter = file_graph.find (sub_file);
			if ((file_iter != file_graph.end()) && (file_iter->second.size()))
			{
				SubCreation (SubProxySet, file_iter->second, file_graph);
			}
		}
	}
}


vector<string> ProxyHandler::getParentProxys (const string &file) const
{
	vector<string> Parents;
	if (isProxy(file))
	{
		return Parents;
	}
	for (auto& file_el : ProxySubTree)
	{
		if (file_el.second.find(file) != file_el.second.end())
		{
			Parents.push_back (file_el.first);
		}
	}
	return Parents;
}

void ProxyHandler::MinimizeProxyMappingFromSelf (graph_type &ProxyMapping) const
{
	for (auto proxy_iter (ProxyMapping.begin()); proxy_iter != ProxyMapping.end(); ++proxy_iter)
	{
		auto& proxy_set = proxy_iter->second;
		Searcher< set<string> > searcher_set {proxy_set};
		auto sub_proxy_iter (proxy_iter);
		++sub_proxy_iter;
		while (sub_proxy_iter != ProxyMapping.end())
		{
			auto& sub_proxy_set = sub_proxy_iter->second;
			searcher_set.flag = true;
			bool found = find_if(sub_proxy_set.begin(), sub_proxy_set.end(), searcher_set) != sub_proxy_set.end();
			if (found)
			{
				searcher_set.flag = false;
				erase_if (sub_proxy_set, searcher_set);
				searcher_set.Set = &sub_proxy_set;
				erase_if (proxy_set, searcher_set);
			}
			++sub_proxy_iter;
		}
	}
}


#include "FreeFunctions.hpp"
#include "graph_type.hpp"
#include <vector>


using namespace std;

string getLabel (const string &full_name)
{
	string label {getPairDir(full_name).first};
	size_t pos_slash;
	if ( (pos_slash = label.find_last_of("/")) != string::npos) {
		if (pos_slash < label.length()-1) {
			label = label.substr(pos_slash+1);
		}
		else {
			label.clear();
		}
	}
	return label;
}

string getPath (const string &full_name)
{
	string Path {getWithoutParent(getPairDir(full_name).first)};
	size_t pos_slash;
	if ( (pos_slash = Path.find_last_of("/")) != string::npos) {
		Path = Path.substr (0, pos_slash);
	}
	return Path;
}

string getWithoutParent (string full_name)
{
	if (full_name.find("/../") == string::npos) {
		return full_name;
	}
	vector<string> repos;
	while (full_name.size())
	{
		string tmp;
		size_t pos = full_name.find_first_of("/");
		tmp = full_name.substr(0, pos);
		full_name = full_name.substr (pos==string::npos ? full_name.size() : pos+1);
		if ( (tmp == "..") && (repos.size()) && repos.back() != ".." ) {
			repos.pop_back();
		}
		else {
			repos.push_back (tmp);
		}
	}
	for (auto& elem : repos) {
		full_name += elem + '/';
	}
	if (full_name.size()) {
		full_name.pop_back();
	}
	return full_name;
}

pair<string, string> getPairDir (string full_name)
{
	pair<string, string> ret_name;
	ret_name.first = full_name.substr(0, full_name.find_first_of (' '));
	ret_name.second = full_name.substr(0, full_name.find_first_of ('>'));
	ret_name.second = ret_name.second.substr(ret_name.second.find_first_of('=')+1);
	return ret_name;
}

string getCanonicalDir (string full_name)
{
	full_name = full_name.substr (0, full_name.find_last_of(':'));
	return full_name.substr (0, full_name.find_last_of(':'));
}

string getCanonical (string full_name)
{
	full_name = getWithoutParent (full_name);
	if (full_name.find('=') != string::npos)
	{
		full_name =  getCanonicalDir (getPairDir(full_name).first);
	}
	else
	{
		full_name = getCanonicalDir (full_name);
	}
	return full_name;
}

int getLineNumber (string location)
{
	location = getPairDir (location).first;
	if (location.find_last_of (":") == string::npos) {
		return 0;
	}
	location = location.substr(0, location.find_last_of (":") );
	location = location.substr(location.find_last_of (":") + 1);
	return std::stoi(location);
}


string subBind (graph_type &Targets, string full_name)
{
	full_name = getWithoutParent (full_name);
	if (full_name.find('=') != string::npos)
	{
		pair<string, string> tmp = getPairDir (full_name);
		tmp.first = getCanonicalDir (tmp.first);
		tmp.second = getCanonicalDir (tmp.second);
		if ((tmp.first != tmp.second) && (!isShadow(full_name)))
		{
			Targets[tmp.first].insert (tmp.second);
		}
		full_name = tmp.first;
	}
	else
	{
		full_name = getCanonicalDir (full_name);
	}
	return full_name;
}




